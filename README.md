<p align="center">![logo](img/logo.png)</p>

A simple color script designed to complement [aurafetch](https://gitlab.com/LionessAlana/aurafetch).

<p align="center">![screenshot](img/screenshot.png)</p>

## Installation
1. `git clone https://gitlab.com/LionessAlana/vivid.git`
2. `cd /usr/bin`
3. `sudo cp -s PATH/TO/VIVID`

Then you can run `vivid` from anywhere.
